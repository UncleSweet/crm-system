package com.unclesweet.crmsystem.settings.mapper;

import com.unclesweet.crmsystem.settings.model.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserMapper {
    int deleteByPrimaryKey(String id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    //根据用户名和密码查询用户
    User selectByLoginActAndLoginPwd(@Param("username") String username, @Param("password") String password);

    //查询所有用户方法
    List<User> selectAll();
}