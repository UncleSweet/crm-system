package com.unclesweet.crmsystem.settings.service.impl;

import com.unclesweet.crmsystem.settings.mapper.UserMapper;
import com.unclesweet.crmsystem.settings.model.User;
import com.unclesweet.crmsystem.settings.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public User queryByLoginActAndLoginPwd(String username, String password) {
        return userMapper.selectByLoginActAndLoginPwd(username,password);
    }

    @Override
    public List<User> queryAll() {
        return userMapper.selectAll();
    }
}
