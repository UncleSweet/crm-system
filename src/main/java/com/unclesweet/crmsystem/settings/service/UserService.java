package com.unclesweet.crmsystem.settings.service;

import com.unclesweet.crmsystem.settings.model.User;

import java.util.List;

public interface UserService {
    //根据用户名和密码查询用户
    User queryByLoginActAndLoginPwd(String username,String password);

    //查询所有用户
    List<User> queryAll();
}
