package com.unclesweet.crmsystem.settings.web;

import com.unclesweet.crmsystem.commons.model.Constants;
import com.unclesweet.crmsystem.commons.model.ResponseResult;
import com.unclesweet.crmsystem.commons.util.DateUtils;
import com.unclesweet.crmsystem.settings.model.User;
import com.unclesweet.crmsystem.settings.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Date;

@Controller
@RequestMapping("/settings/qx/user")
public class UserController {

    @Autowired
    private UserService userService;

    //登录页面
    @RequestMapping("/login")
    public String login(){
        return "settings/qx/user/login";
    }

    //登录逻辑
    @PostMapping("/doLogin")
    @ResponseBody
    public ResponseResult doLogin(String username, String password, Boolean isRememberMe, HttpServletRequest request, HttpServletResponse response,HttpSession session){
        //创建自定义返回结果类型
        ResponseResult responseResult = new ResponseResult();

        //调用业务层查找当前对象
        User user = userService.queryByLoginActAndLoginPwd(username,password);

        //判断是否有此对象
        if(user != null){
            //获取当前时间
            String now = DateUtils.formatDateTime(new Date());
            //判断是否过期，当前时间大于数据库定义时间则表示超时
            if(now.compareTo(user.getExpireTime()) > 0){
                responseResult.setCode(Constants.STATUS_EXPIRED);
                responseResult.setMsg("当前账号已过期");
            }
            //判断是否锁住
            else if(user.getLockState().equals("0")){
                responseResult.setCode(Constants.STATUS_LOCKED);
                responseResult.setMsg("当前账户被冻结");
            }
            //判断是否ip受限制
            else{
                //获取当前ip地址
                String ip = request.getRemoteAddr();
                //判断当前用户的ip地址范围是否包含当前请求ip地址
                if(user.getAllowIps().contains(ip)){
                    responseResult.setCode(Constants.STATUS_OK);
                    responseResult.setMsg("登录成功");

                    //保存当前用户信息到session
                    session.setAttribute(Constants.SESSION_USER,user);

                    //判断是否选择了10天直接登录
                    if(isRememberMe){
                        Cookie cookieUserName = new Cookie("username",user.getLoginAct());
                        Cookie cookiePassword = new Cookie("password",user.getLoginPwd());

                        cookieUserName.setMaxAge(10*24*60*60);
                        cookiePassword.setMaxAge(10*24*60*60);

                        response.addCookie(cookieUserName);
                        response.addCookie(cookiePassword);
                    }
                }
                else{
                    responseResult.setCode(Constants.STATUS_IP);
                    responseResult.setMsg("当前IP地址权限不足，无法登录");
                }
            }
        }else{
            responseResult.setCode(Constants.STATUS_LOGINERROR);
            responseResult.setMsg("用户名或者密码错误");
        }

        //返回当前对象
        return responseResult;
    }

    @RequestMapping("/doLogout")
    public String doLogout(HttpServletRequest request,HttpSession session,HttpServletResponse response){
        //清空session
        session.removeAttribute(Constants.SESSION_USER);
        //获取Cookie数组
        Cookie[] cookies = request.getCookies();
        //循环cookies并且删除
        for(Cookie cookie : cookies){
            if(cookie.getName().equals("username") || cookie.getName().equals("password")){
                //设置立马过期
                cookie.setMaxAge(0);
                //重新发送cookie到前端
                response.addCookie(cookie);
            }
        }
        //重定向到登录页面
        return "redirect:/settings/qx/user/login";
    }
}
