package com.unclesweet.crmsystem.activity.mapper;

import com.unclesweet.crmsystem.activity.model.Activity;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ActivityMapper {
    int deleteByPrimaryKey(String id);

    int insert(Activity record);

    int insertSelective(Activity record);

    Activity selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(Activity record);

    int updateByPrimaryKey(Activity record);

    //根据活动名，组织者，起始事件，结束事件查询数据并且分页
    List<Activity> selectConditionWithPage(@Param("name") String name,@Param("owner") String owner,@Param("startDate") String startDate,@Param("endDate") String endDate,@Param("startIndex") int startIndex,@Param("pageSize") int pageSize);

    //根据活动名，组织者，起始事件，结束事件查询数据数量
    int selectConditionCount(@Param("name") String name,@Param("owner") String owner,@Param("startDate") String startDate,@Param("endDate") String endDate);

    //根据id集合批量删除活动
    int deleteBatchByIds(String[] ids);

    //查询所有活动数据
    List<Activity> selectAll();

    //根据id集合查询活动数据
    List<Activity> selectBatchByIds(String[] ids);

    //根据活动数据集合批量插入数据库
    int insertBatchByActivities(List<Activity> activities);
}