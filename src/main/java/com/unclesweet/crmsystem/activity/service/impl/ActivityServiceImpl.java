package com.unclesweet.crmsystem.activity.service.impl;

import com.unclesweet.crmsystem.activity.mapper.ActivityMapper;
import com.unclesweet.crmsystem.activity.model.Activity;
import com.unclesweet.crmsystem.activity.service.ActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActivityServiceImpl implements ActivityService {

    @Autowired
    private ActivityMapper activityMapper;

    @Override
    public List<Activity> queryConditionWithPage(String name, String owner, String startDate, String endDate, int startIndex, int pageSize) {
        return activityMapper.selectConditionWithPage(name,owner,startDate,endDate,startIndex,pageSize);
    }

    @Override
    public int queryConditionCount(String name, String owner, String startDate, String endDate) {
        return activityMapper.selectConditionCount(name,owner,startDate,endDate);
    }

    @Override
    public int addActivity(Activity activity) {
        return activityMapper.insertSelective(activity);
    }

    @Override
    public Activity queryById(String id) {
        return activityMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateActivity(Activity activity) {
        return activityMapper.updateByPrimaryKeySelective(activity);
    }

    @Override
    public int deleteBatchByIds(String[] ids) {
        return activityMapper.deleteBatchByIds(ids);
    }

    @Override
    public List<Activity> queryAll() {
        return activityMapper.selectAll();
    }

    @Override
    public List<Activity> queryBatchByIds(String[] ids) {
        return activityMapper.selectBatchByIds(ids);
    }

    @Override
    public int addBatchByActivities(List<Activity> activities) {
        return activityMapper.insertBatchByActivities(activities);
    }

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("spring-config.xml");
        ActivityService activityService = context.getBean(ActivityService.class);
        /*int page = 1;
        int pageSize = 10;
        String name = "";
        String owner = "";
        String startDate = "";
        String endDate = "";
        int startIndex = (page - 1)*pageSize;

        //查询数据
        System.out.println(activityService.queryConditionWithPage(name, owner, startDate, endDate, startIndex, pageSize));
        System.out.println(activityService.queryConditionWithPage(name, owner, startDate, endDate, startIndex, pageSize).size());
        //查询数量
        System.out.println(activityService.queryConditionCount(name, owner, startDate, endDate));*/

        /*//批量删除数据
        String[] ids = {"abf6ba51d0784a86a26cae142f163419","b9e2b2b5408645b3bf2f7bcba94cb3cf"};
        activityService.deleteBatchByIds(ids);
        System.out.println(ids.length);*/

        //批量查找数据
        /*String[] ids = {"06f5fc056eac41558a964f96daa7f27b","5a85ab8bfe1e4639adf838a643cbdf9f"};
        List<Activity> activities = activityService.queryBatchByIds(ids);
        System.out.println(activities.toString());
        System.out.println(ids.length);*/
    }

}
