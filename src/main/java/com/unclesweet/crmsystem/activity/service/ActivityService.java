package com.unclesweet.crmsystem.activity.service;

import com.unclesweet.crmsystem.activity.model.Activity;

import java.util.List;

public interface ActivityService {

    //根据活动名，组织者，起始事件，结束事件查询数据并且分页
    List<Activity> queryConditionWithPage(String name,String owner,String startDate,String endDate,int startIndex,int pageSize);

    //根据活动名，组织者，起始事件，结束事件查询数据数量
    int queryConditionCount(String name,String owner,String startDate,String endDate);

    //创建活动
    int addActivity(Activity activity);

    //根据活动id查询单个活动对象
    Activity queryById(String id);

    //修改活动
    int updateActivity(Activity activity);

    //根据id集合批量删除活动
    int deleteBatchByIds(String[] ids);

    //查询所有活动数据
    List<Activity> queryAll();

    //根据id集合查询活动数据
    List<Activity> queryBatchByIds(String[] ids);

    //根据活动数据集合批量插入数据库
    int addBatchByActivities(List<Activity> activities);
}
