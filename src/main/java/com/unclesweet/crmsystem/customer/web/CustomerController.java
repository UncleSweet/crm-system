package com.unclesweet.crmsystem.customer.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/workbench/customer")
public class CustomerController {

    @RequestMapping("/index")
    public String index(){
        return "workbench/customer/index";
    }
}
