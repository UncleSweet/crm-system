package com.unclesweet.crmsystem.transaction.mapper;

import com.unclesweet.crmsystem.transaction.model.TransactionRemark;

public interface TransactionRemarkMapper {
    int deleteByPrimaryKey(String id);

    int insert(TransactionRemark record);

    int insertSelective(TransactionRemark record);

    TransactionRemark selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(TransactionRemark record);

    int updateByPrimaryKey(TransactionRemark record);
}