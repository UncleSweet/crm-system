package com.unclesweet.crmsystem.transaction.mapper;

import com.unclesweet.crmsystem.transaction.model.TransactionHistory;

public interface TransactionHistoryMapper {
    int deleteByPrimaryKey(String id);

    int insert(TransactionHistory record);

    int insertSelective(TransactionHistory record);

    TransactionHistory selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(TransactionHistory record);

    int updateByPrimaryKey(TransactionHistory record);
}