package com.unclesweet.crmsystem.transaction.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/workbench/transaction")
public class TransactionController {

    @RequestMapping("/index")
    public String index(){
        return "workbench/transaction/index";
    }
}
