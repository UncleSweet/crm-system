package com.unclesweet.crmsystem.interceptor;

import com.unclesweet.crmsystem.commons.model.Constants;
import com.unclesweet.crmsystem.settings.model.User;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class SecurityInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        //获取session
        User user = (User) request.getSession().getAttribute(Constants.SESSION_USER);
        if(user==null){
            response.sendRedirect("/settings/qx/user/login");
            return false;
        }
        return true;
    }
}
