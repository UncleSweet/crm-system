package com.unclesweet.crmsystem.clue.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/workbench/clue")
public class ClueController {

    @RequestMapping("/index")
    public String index(){
        return "workbench/clue/index";
    }

}
