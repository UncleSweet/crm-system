package com.unclesweet.crmsystem.commons.util;

import java.util.UUID;

public class UUIDUtils {

    //生成32为UUID静态方法,去除横杠
    public static String getUUID(){
        return UUID.randomUUID().toString().replaceAll("-","");
    }

}
