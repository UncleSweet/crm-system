package com.unclesweet.crmsystem.commons.util;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtils {

    //转换日期格式静态方法
    public static String formatDateTime(Date date){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time = simpleDateFormat.format(date);
        return time;
    }
}
