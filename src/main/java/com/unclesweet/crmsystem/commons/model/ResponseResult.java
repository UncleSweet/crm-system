package com.unclesweet.crmsystem.commons.model;

/**
 * 自定义结果类型
 */
public class ResponseResult {

    private int code;       //状态码
    private String msg;     //提示信息
    private Object data;    //返回数据

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
