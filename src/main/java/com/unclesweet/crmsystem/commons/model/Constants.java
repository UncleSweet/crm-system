package com.unclesweet.crmsystem.commons.model;

/**
 * 常量定义
 */
public class Constants {

    public static final int STATUS_OK = 2000;           //成功
    public static final int STATUS_ERROR = 2001;        //失败
    public static final int STATUS_LOGINERROR = 2002;   //用户名或者密码错误
    public static final int STATUS_EXPIRED = 2003;      //账户过期
    public static final int STATUS_LOCKED = 2004;       //账户冻结
    public static final int STATUS_IP = 2005;           //账户IP受限
    public static final int STATUS_ACCESSERROR = 2006;  //权限不足

    public static final String SESSION_USER = "user";
    public static final String MESSAGE = "message";

}
