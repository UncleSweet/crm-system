package com.unclesweet.crmsystem.contacts.mapper;

import com.unclesweet.crmsystem.contacts.model.Contacts;
import com.unclesweet.crmsystem.contacts.model.ContactsKey;

public interface ContactsMapper {
    int deleteByPrimaryKey(ContactsKey key);

    int insert(Contacts record);

    int insertSelective(Contacts record);

    Contacts selectByPrimaryKey(ContactsKey key);

    int updateByPrimaryKeySelective(Contacts record);

    int updateByPrimaryKey(Contacts record);
}