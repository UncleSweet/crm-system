package com.unclesweet.crmsystem.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {

    //pages的主页，为了能用同一个身份请求内部，否则需要使用token
    @RequestMapping("/")
    public String index(){
        return "index";
    }

    //CRM的主页
    @RequestMapping("/index")
    public String home(){
        return "workbench/index";
    }

    //CRM主页请求图片页面
    @RequestMapping("/main")
    public String main(){
        return "workbench/main/index";
    }

}
