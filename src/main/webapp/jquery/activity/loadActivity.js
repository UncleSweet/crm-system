$(function(){
    loadActivity(1,4);

    //活动数据查询事件
    $("#searchActivity").click(function(){
        //获取当前页面插件存储条目数
        let pageSize = $("#bsPagination").bs_pagination('getOption','rowsPerPage');
        loadActivity(1,pageSize);
    });

    //加载日历插件
    $(".myDate").datetimepicker({
        language:"zh-CN",
        format:"yyyy-mm-dd", //格式
        minView:'month',  //最小单位，月
        initialDate:new Date(), //初始化显示日期为当天
        autoclose:true, //自动关闭
        todayBtn:true, //显示当前日期按钮
        clearBtn:true, //显示清空按钮
        ignoreReadonly:true //设置不可编辑
    });

    //创建活动按钮事件
    $("#btn_createActivity").click(function(){
        //模态弹出
        $("#createActivityModal").modal("show");
        //加载用户列表
        $.ajax({
           url:"/workbench/activity/allUser",
           dataType:"json",
           method:"get",
           success:function(data){
               if(data.code == "2000"){
                   let html = `<option value="0">请选择</option>`;
                   $.each(data.data,function(i,item){
                       html += `<option value=${item.id}>${item.name}</option>`;
                   });
                   $("#create-marketActivityOwner").html(html);
               }
           }
        });
    });

    //保存活动事件
    $("#btn_saveActivity").click(function(){
        let owner = $("#create-marketActivityOwner").val();
        let name = $("#create-marketActivityName").val();
        let startTime = $("#create-startTime").val();
        let endTime = $("#create-endTime").val();
        let cost = $("#create-cost").val();
        let description = $("#create-describe").val();

        if(checkActivity(owner,name,startTime,endTime,cost)){
            //发送请求
            $.ajax({
                url:"/workbench/activity/addActivity",
                data:{
                    owner : owner,
                    name : name,
                    startDate : startTime,
                    endDate : endTime,
                    cost : cost,
                    description : description
                },
                dataType:"json",
                method:"post",
                success:function(data){
                    if(data.code == "2000"){
                        alert(data.msg);
                        //清空模态框表单中所有内容
                        $("#createActivityForm")[0].reset();
                        //关闭模态框
                        $("#createActivityModal").modal("hide");
                        //重新加载数据
                        loadActivity(1,$("#bsPagination").bs_pagination('getOption','rowsPerPage'));
                    }else{
                        alert(data.msg);
                    }
                },
                error:function(e,xhr){
                    console.log(e.status);
                    console.log(xhr);
                }
            })
        }
    });

    //关闭活动事件
    $("#btn_closeCreate").click(function () {
        //清空模态框中所有内容
        $("span").html("");
        //关闭模态框
        $("#createActivityModal").modal("hide");
    })

    //修改活动按钮事件
    $("#btn_editActivity").click(function(){
        //获取复选框的按钮的数量
        let checkbox = $(".ck:checked");
        //判断是否只选择一个
        if(checkbox.size() != 1){
            alert("只能选择并且最多选择一个");
        }else{
            //发送请求获取当前活动的数据
            $.ajax({
                url:"/workbench/activity/oneActivity",
                data:{id:checkbox.get(0).value},
                dataType:"json",
                method:"get",
                success:function(data){
                    let activity = data.data;
                    if(data.code = "2000"){
                        $("#edit-activityId").val(activity.id);
                        $("#edit-marketActivityName").val(activity.name);
                        $("#edit-startTime").val(activity.startDate);
                        $("#edit-endTime").val(activity.endDate);
                        $("#edit-cost").val(activity.cost);
                        $("#edit-describe").val(activity.description);
                        //打开修改窗口
                        $("#editActivityModal").modal("show");

                        //发送请求获取所有者列表并且设置当前为默认选择
                        $.ajax({
                            url:"/workbench/activity/allUser",
                            dataType:"json",
                            method:"get",
                            success:function(data){
                                if(data.code = "2000"){
                                    let html = "";
                                    $.each(data.data,function(i,item){
                                        if(item.id == activity.owner){
                                            html += `<option value="${item.id}" selected>${item.name}</option>`;
                                        }else {
                                            html += `<option value="${item.id}">${item.name}</option>`;
                                        }
                                    });
                                    $("#edit-marketActivityOwner").html(html);
                                }else{
                                    alert(data.msg);
                                }
                            }
                        })
                    }else{
                        alert(data.msg);
                    }
                }
            });
        }
    });

    //更新活动按钮事件
    $("#btn_updateActivity").click(function(){
        //获取数据
        let id = $("#edit-activityId").val();
        let owner = $("#edit-marketActivityOwner").val();
        let name = $("#edit-marketActivityName").val();
        let startTime = $("#edit-startTime").val();
        let endTime = $("#edit-endTime").val();
        let cost = $("#edit-cost").val();
        let description = $("#edit-describe").val();

        //表单验证
        if(checkActivity(owner,name,startTime,endTime,cost)){
            //发送请求
            $.ajax({
                url:"/workbench/activity/editActivity",
                data:{
                    id:id,
                    owner : owner,
                    name : name,
                    startDate : startTime,
                    endDate : endTime,
                    cost : cost,
                    description : description
                },
                dataType:"json",
                method:"post",
                success:function(data){
                    if(data.code == "2000"){
                        alert(data.msg);
                        //清空模态框中所有内容
                        $("#editActivityForm")[0].reset();
                        //关闭模态框
                        $("#editActivityModal").modal("hide");
                        //重新加载数据
                        loadActivity(1,$("#bsPagination").bs_pagination('getOption','rowsPerPage'));
                    }else{
                        alert(data.msg);
                    }
                },
                error:function(e,xhr){
                    console.log(e.status);
                    console.log(xhr);
                }
            })
        }
    });

    //全选按钮控制复选框事件
    $("#ckAll").click(function(){
        //获取当前全选框的选择状态
        let checked = $("#ckAll").prop("checked");
        //同步设置到下面所有复选框
        $(".ck").prop("checked",checked);
    });

    //复选框控制全选按钮事件
    $("#loadActivity").on('click','.ck',function(){
        //获取复选框的数量
        let allSize = $(".ck").size();
        //获取当前复选框选中的数量
        let checkedSize = $(".ck:checked").size();
        //判断两者是否相等
        if(allSize == checkedSize){
            $("#ckAll").prop("checked",true);
        }else{
            $("#ckAll").prop("checked",false);
        }
    })

    //删除按钮点击事件
    $("#btn_deleteActivity").click(function(){
        //获取选中的复选框数量
        let checked = $(".ck:checked");
        if(checked.size() <= 0){
            alert("最少选中一个或者选中多个！");
        }else{
            //定义id数组
            let ids = new Array();
            //循环获取这些复选框的值
            $.each(checked,function(i,item){
                ids.push($(item).val());
            })

            //发送异步请求执行批量删除
            $.ajax({
                url:"/workbench/activity/batchDelete",
                type:"GET",
                data:{ids:ids},
                dataType:"json",
                traditional:true,
                success: function(data){
                    if(data.code = "2000"){
                        alert(data.msg);
                    }else {
                        alert(data.msg);
                    }
                    //清空条件查询
                    $("#conditionActivityForm")[0].reset();
                    //清除复选框内容并且重新加载数据
                    $("#ckAll").prop('checked', false);
                    $(".ck:checked").prop('checked', false);
                    //重新加载数据
                    loadActivity(1,$("#bsPagination").bs_pagination('getOption','rowsPerPage'));
                },
                error:function(e,xhr){
                    console.log(e.status);
                    console.log(xhr);
                }
            });
        }
    })

    //批量导出活动数据事件
    $("#exportActivityAllBtn").click(function () {
        window.location.href = "/workbench/activity/exportActivity";
    })

    //选择导出活动数据事件
    $("#exportActivityXzBtn").click(function(){
        //获取复选框的对象
        let checked = $(".ck:checked");
        if(checked.size() <= 0){
            alert("必须选择数据！");
        }else {
            //定义id数组
            let ids = "";

            //循环获取选中复选框的值
            $.each(checked, function (i, item) {
                ids += "&ids="+$(item).val();
            })
            //截取开头部分信息
            ids = ids.substring(5);
            //发送请求
            window.location.href = "/workbench/activity/exportActivity?ids="+ids;
        }
    })

    //导入模态框导入按钮事件
    $("#importActivityBtn").click(function(){
        //获取上传文件名称
        var filename = $("#activityFile").val();
        //获取文件扩展名
        var extension = filename.substring(filename.lastIndexOf(".")+1).toUpperCase();

        //判断是否为excel文件
        if(extension != "XLS" && extension != "XLSX"){
            alert("上传文件不为excel表格，请重新选择");
            return ;
        }

        //获取第一个文件对象，并且判断大小
        var activityFile = $("#activityFile")[0].files[0];
        if(activityFile.size > 1024*1024*5){
            alert("文件不能超过5MB");
            return ;
        }

        //将文件打包铖表单对象
        var formData = new FormData();
        formData.append("multipartFile",activityFile);
        //发送请求
        $.ajax({
            url:"/workbench/activity/importActivity",
            data:formData,
            dataType:"json",
            method:"POST",
            //设置contentType为false，表示发送请求时阻止JQ对文件添加操作符号，从而失去识别功能。
            contentType:false,
            //设置processData为false，表示发送数据时不转换成查询字符串。
            processData:false,
            success:function(data){
                if(data.code == 2000){
                    alert(data.msg);
                    //重新加载数据
                    loadActivity(1,$("#bsPagination").bs_pagination('getOption','rowsPerPage'));
                    //关闭模态框
                    $("#importActivityModal").modal('hide');
                }else{
                    alert(data.msg);
                    $("#importActivityModal").modal('show');
                }
            },
            error:function(xhr,error){
                alert(error);
            }
        })
    })

});

//加载活动数据函数
function loadActivity(pageNo,pageSize){
    let name = $("#name").val();
    let owner = $("#owner").val();
    let startDate = $("#startDate").val();
    let endDate = $("#endDate").val();

    $.getJSON("/workbench/activity/loadActivity",{
        name : name,
        owner : owner,
        startDate : startDate,
        endDate : endDate,
        page : pageNo,
        pageSize : pageSize
    },function(data){
        var html = "";
        $.each(data.data.activities,function(i,item){
            html += `<tr class="active">`;
            html += `<td><input type="checkbox" class="ck" value="${item.id}"/></td>`;
            html += `<td><a style="text-decoration: none;cursor: pointer;">${item.name}</a></td>`;
            html += `<td>${item.owner}</td>`;
            html += `<td>${item.startDate}</td>`;
            html += `<td>${item.endDate}</td>`;
            html += `</tr>`;
        });
        $("#loadActivity").html(html);

        //分页操作
        //计算总页数
        var totalPage = Math.ceil(data.data.count/pageSize);
        $("#bsPagination").bs_pagination({
            currentPage:pageNo,
            rowsPerPage:pageSize,
            totalPages:totalPage,
            totalRows:data.data.count,
            visiblePageLinks: 4,    //显示翻页卡的卡数
            showGoToPage: false,
            showRowsPerPage: true,  //是否显示每页显示的条目数

            onChangePage:function(event,object){
                //获取当前跳转页面的页码和条目数
                loadActivity(object.currentPage,object.rowsPerPage);
                //清空全选按钮
                $("#ckAll").prop("checked",false);
            }
        })
    });
}

//活动表单验证函数
function checkActivity(owner,name,startTime,endTime,cost){
    let flag = true;
    //判断所有者
    if(owner == '0'){
        $("#ownerTips").html("所有者不能为空");
        flag = false;
    }else{
        $("#ownerTips").html("");
    }

    //判断活动名称
    if(name == ''){
        $("#nameTips").html("活动名称不能为空");
        flag = false;
    }else{
        $("#nameTips").html("");
    }

    //判断日期
    if(startTime == '' || endTime == ''){
        $("#dataTips").html("起始时间或者结束时间不能为空");
        flag = false;
    }else if(new Date(endTime).getTime() < new Date(startTime).getTime()){
        $("#dataTips").html("结束日期不能小于起始日期");
        flag = false;
    }else{
        $("#dataTips").html("");
    }

    //判断成本
    if(!/^[+]{0,1}(\d+)$|^[+]{0,1}(\d+\.\d+)$/.test(cost)) {
        $("#costTips").html("成本不能为负数或者非数字");
        flag = false;
    }else{
        $("#costTips").html("");
    }

    return flag;
}