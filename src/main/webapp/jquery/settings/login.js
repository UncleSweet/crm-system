$(function(){
    //登录按钮点击事件
    $("#btn_login").click(function(){
        var username = $("#username").val();
        var password = $("#password").val();
        var isRememberMe = $("#isRememberMe").prop("checked");

        //判断当前密码是否为32位，是则不加密
        if(password.length!=32){
            password = $.md5($("#password").val());
        }

        console.log(username);
        console.log(password);
        console.log(isRememberMe);

        //判断用户名和密码是否为空
        if(username=="" && password==""){
            $("#tips").innerText="用户名和密码不能为空";
        }else{
            $.ajax({
                url:"/settings/qx/user/doLogin",
                data:{username:username,password:password,isRememberMe:isRememberMe},
                type:"post",
                success:function(data){
                    //判断返回状态值是否位2000，是则表示登录成功跳转主页
                    if(data.code == 2000){
                        window.location.href="/index";
                    }else{
                        $("#tips").html(data.msg);
                    }
                },
                error:function(e,xhr){
                    console.log(xhr);
                }

            })
        }
    })
})